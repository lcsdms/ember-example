import DS from 'ember-data';

export default DS.Model.extend({
  hotel:DS.belongsTo('hotel',{async: false}),
  code:DS.attr('string'),
  name: DS.attr('string'),
  description: DS.attr('string'),
  items: DS.belongsTo('contents', { inverse: null }) //todo Verificar se a relação reflexiva é essa mesmo https://guides.emberjs.com/v2.0.0/models/defining-models/#toc_reflexive-relations
  //todo verificar se esta relacao tambem se aplica a one to many
});
