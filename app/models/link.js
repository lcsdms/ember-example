import DS from 'ember-data';

export default DS.Model.extend({
  hotel: DS.belongsTo('hotel', {async:false}),
  thumbnailImage: DS.belongsTo('image', {async:false,inverse:'thumbnailImage'}),
  images: DS.hasMany('image')
});
