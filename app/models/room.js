import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  description: DS.attr('string'),
  group: DS.attr(''), //todo verificar como interpretar esse valor : 204778755|Sem café|true, implementar serializer
  mealPlan: DS.attr('string'),
  category: DS.attr('string'),
  minPax: DS.attr('number'),
  maxPax: DS.attr('number'),
  quantityAvaiable: DS.attr('number'),
  isAvaiable: DS.attr('boolean'),
  rph: DS.attr('number'),
  cancellationPolicy: DS.belongsTo('cancellationPolicy', {async:false}),
  rates: DS.hasMany('rates'), //todo verificar array
  contents: DS.attr() // todo verificar array
});
