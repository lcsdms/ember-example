import DS from 'ember-data';

export default DS.Model.extend({
  hotel: DS.belongsTo('hotel', {async: false}),
  address: DS.attr('string'),
  coordinates: DS.belongsTo('coordinates', {async:false})
});
