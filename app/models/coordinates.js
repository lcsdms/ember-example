import DS from 'ember-data';

export default DS.Model.extend({
  location: DS.belongsTo('location',{async: false}),
  latitude: DS.attr('string'),
  longitude: DS.attr('string')
});
