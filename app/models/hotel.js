import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  description: DS.attr('string'),
  award: DS.attr('number'),
  isPreferential: DS.attr('boolean'),
  location: DS.belongsTo('location', {async: false}),
  contents: DS.belongsTo('contents',{async: false}),
  links:DS.belongsTo('link', {async:false})
});
