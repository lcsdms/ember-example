import DS from 'ember-data';

export default DS.Model.extend({
  link: DS.belongsTo('link',{async:false}),
  thumbnailImage: DS.belongsTo('link', {async:false}),
  href: DS.attr('string')
});
