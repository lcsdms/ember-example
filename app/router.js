import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('hotel',{path: '/hotel/:hotel_id'});
  this.route('quartos');
  this.route('hoteis');
  this.route('quarto');
});

export default Router;
