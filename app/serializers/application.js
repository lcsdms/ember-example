import Ember from 'ember';
import RESTSerializer from 'ember-data/serializers/rest';

export default RESTSerializer.extend({
  //Se o objeto não possui ID, gera um para o mesmo, possibilitando o uso de embedded properties sem ID como models
  extractId(modelClass, resourceHash) {
    if (Ember.isEmpty(resourceHash.id)) {
      return Ember.guidFor(resourceHash);
    }
    return this._super(modelClass, resourceHash);
  }
});
