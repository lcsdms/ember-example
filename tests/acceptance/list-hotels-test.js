import { test } from 'qunit';
import moduleForAcceptance from 'ember-example/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | list hotels');

test('Deve redirecionar para a rota de hoteis', function(assert) {
  visit('/');

  andThen(function() {
    assert.equal(currentURL(), '/hoteis', 'deve redirecionar automaticamente');
  });
});



